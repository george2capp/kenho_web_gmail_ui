import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailSignInTest {
    WebDriver driver = new FirefoxDriver();

    @Test
    public void gmailLoginShouldbeSuccessfull() throws InterruptedException {
        //1. go to gmail website

        driver.get("http://gmail.com");
        //Fill in username
        WebElement userName = driver.findElement(By.id("identifierId"));
        userName.clear();
        userName.sendKeys("romancegm");
        //click next button
        WebElement nextButton = driver.findElement(By.xpath("//span[contains(text(), 'Next')]"));
        nextButton.click();
        Thread.sleep(2000);
        //click password textfiled
        WebElement passwordName = driver.findElement(By.xpath("//*[@id='password']//input"));
        passwordName.clear();
        passwordName.sendKeys("jovanka99");
        // click next button
        WebElement nextButton1 = driver.findElement(By.xpath("//span[contains(text(), 'Next')]"));
        nextButton1.click();
        // verify user did sign in
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Inbox")));
        Assert.assertTrue("Inbox should exist", driver.findElements(By.partialLinkText("Inbox")).size() > 0);
        // sign out.
        WebElement profileButton = driver.findElement(By.xpath("//span[@class='gb_9a gbii']"));
        profileButton.click();

        WebElement signOutLinkage = driver.findElement(By.linkText("Sign out"));
        signOutLinkage.click();

        // verify user signed out.
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='passwordNext']/content/span")));
        Assert.assertTrue("Next button should exist",
                driver.findElements(By.xpath("//div[@id='passwordNext']/content/span")).size() > 0);
    }

    @After
    public void tearDown(){
        driver.close();
    }
}
